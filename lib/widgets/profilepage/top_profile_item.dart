import 'package:flutter/material.dart';

class TopProfileItem extends StatelessWidget {
  const TopProfileItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        //membuat foto profil user
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [Colors.purple, Colors.red, Colors.orange]),
                    borderRadius: BorderRadius.circular(50)),
              ),
              Container(
                height: 95,
                width: 95,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage('https://picsum.photos/536/354'),
                        fit: BoxFit.cover),
                    color: Colors.grey[300],
                    border: Border.all(color: Colors.black, width: 4),
                    borderRadius: BorderRadius.circular(47)),
              )
            ],
          ),
        ),
        Row(
            //membuat daftar postingan,pengikut,danmengikuti
            children: [
              Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Text('1', style: TextStyle(color: Colors.white)),
                    Text(
                      'Postingan',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Text('999', style: TextStyle(color: Colors.white)),
                    Text(
                      'Pengikut',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Text('2987', style: TextStyle(color: Colors.white)),
                    Text(
                      'Mengikuti',
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
              )
            ])
      ],
    );
  }
}
