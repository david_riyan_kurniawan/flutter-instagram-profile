import 'package:flutter/material.dart';

class RowContentItem extends StatefulWidget {
  RowContentItem(this.active, this.icon);
  final bool active;
  final IconData icon;

  @override
  State<RowContentItem> createState() => _RowContentItemState();
}

class _RowContentItemState extends State<RowContentItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width * 1 / 2,
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    width: 2,
                    //membuat percabangan
                    color: widget.active == true
                        ? Colors.white
                        : Color.fromARGB(255, 87, 85, 85)))),
        child: Icon(
          widget.icon,
          size: 35,
          color: Colors.white,
        ),
      ),
    );
  }
}
