import 'package:flutter/material.dart';

class ListContentItem extends StatelessWidget {
  const ListContentItem({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 15,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3, crossAxisSpacing: 5, mainAxisSpacing: 5),
      itemBuilder: (context, index) => Image.network(
        'https://picsum.photos/536/200',
        fit: BoxFit.cover,
      ),
    );
  }
}
