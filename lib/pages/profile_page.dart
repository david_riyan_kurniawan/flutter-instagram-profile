import 'package:flutter/material.dart';
import '../widgets/profilepage/top_profile_item.dart';
import '../widgets/profilepage/edit_profile_item.dart';
import '../widgets/profilepage/saran_following_item.dart';
import '../widgets/profilepage/sorotan_story_item.dart';
import '../widgets/profilepage/row_content_item.dart';
import '../widgets/profilepage/list_content_item.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  late int index;

  void initState() {
    // TODO: implement initState
    index = 4;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Row(
            children: const [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Icon(Icons.lock_outline_rounded),
              ),
              Text('Drk_22drynnnnn'),
              Icon(Icons.arrow_drop_down_sharp)
            ],
          ),
          actions: [
            IconButton(onPressed: () {}, icon: Icon(Icons.add_box_outlined)),
            IconButton(onPressed: () {}, icon: Icon(Icons.menu))
          ],
        ),
        body: ListView(
          children: [
            TopProfileItem(),
            Padding(
              padding: const EdgeInsets.all(10),
              child: EditProfileItem(),
            ),
            // SaranFollowingItem(),
            SorotanStoryItem(),
            Row(
              children: [
                RowContentItem(true, Icons.grid_on_sharp),
                RowContentItem(false, Icons.person_pin_outlined),
              ],
            ),
            ListContentItem()
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Colors.black,
          onTap: (value) {
            setState(() {
              index = value;
            });
          },
          elevation: 0,
          currentIndex: index,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: [
            BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(
                Icons.home,
              ),
              label: 'home',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(Icons.search),
              label: 'search',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(Icons.movie_creation),
              label: 'reels',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(Icons.shopping_bag_outlined),
              label: 'shop',
            ),
            BottomNavigationBarItem(
              backgroundColor: Colors.black,
              icon: Icon(Icons.person),
              label: 'profile',
            )
          ],
        ),
      ),
    );
  }
}
